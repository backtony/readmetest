<div align="center">
 <img src="/main.PNG" alt="gjgs-main-image">
</div>

<div align="center">

 <table class="center">
    <tr>dd</tr>
  </table>

### 팀명 : 가지각색
 </div>

 <div align="center">

|&nbsp; &nbsp;&nbsp; &nbsp; 고범석 &nbsp; &nbsp;&nbsp; &nbsp;<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> | 최준성 | 김기완
|:---:|:---:|:---:|  
| - 팀장 <br> - Back-end <br>- DevOps | - 기획자 <br> - Back-end<br>- DevOps | - PM <br>- Front-end

 </div>
 
### Languages
![HTML5](https://img.shields.io/badge/-HTML5-000000?style=flat&logo=HTML5)
![HTML5](https://img.shields.io/badge/-CSS3-000000?style=flat&logo=CSS3)
![JavaScript](https://img.shields.io/badge/-JavaScript-000000?style=flat&logo=javascript)
![TypeScript](https://img.shields.io/badge/-TypeScript-000000?style=flat&logo=typescript&logoColor=007ACC)
![Java](https://img.shields.io/badge/-Java-000000?style=flat&logo=Java&logoColor=007396)
![SQL](https://img.shields.io/badge/-SQL-000000?style=flat&logo=MySQL)


### Technologies
![Git](https://img.shields.io/badge/-Git-000000?style=flat&logo=git&logoColor=F05032)
![GitHub](https://img.shields.io/badge/-GitHub-000000?style=flat&logo=github&logoColor=FFFFFF)
![GitLab](https://img.shields.io/badge/GitLab-000000?style=flat&logo=gitlab&logoColor=white)
![Jira](https://img.shields.io/badge/-Jira-000000?style=flat&logo=jira-software&logoColor=white&logoColor=0052CC)
![AWS](https://img.shields.io/badge/-AWS-000?&logo=Amazon-AWS&logoColor=F90)
![Linux](https://img.shields.io/badge/-Linux-000?&logo=Linux)
![Docker](https://img.shields.io/badge/-Docker-000?&logo=Docker)
![React](https://img.shields.io/badge/-React-000?&logo=React)
![Redis](https://img.shields.io/badge/-Redis-000?&logo=Redis)
![Spring](https://img.shields.io/badge/-Spring-000?&logo=Spring)
![MySQL](https://img.shields.io/badge/-MySQL-000?&logo=Mysql)



